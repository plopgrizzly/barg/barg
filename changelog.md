## 0.0.3
* Update to fonterator version 0.3

## 0.0.2
* Fix fonterator problems.

## 0.0.1
* Initial release.
